package com.bda.lesson36;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class Lesson36Application {

    public static void main(String[] args) {
        SpringApplication.run(Lesson36Application.class, args);
    }

}
