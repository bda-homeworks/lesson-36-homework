package com.bda.lesson36.service.impl;

import com.bda.lesson36.model.Role;
import com.bda.lesson36.model.User;
import com.bda.lesson36.repository.UserRepository;
import com.bda.lesson36.service.UserService;
import com.bda.lesson36.web.dto.UserRegistrationDTO;
import org.springframework.stereotype.Service;
import java.util.Arrays;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public User save(UserRegistrationDTO userRegistrationDTO) {
        User user = new User(userRegistrationDTO.getFirstName(),
                userRegistrationDTO.getLastName(),
                userRegistrationDTO.getEmail(),
                userRegistrationDTO.getPassword(),
                Arrays.asList(new Role("ROLE_USER")));

        return userRepository.save(user);
    }
}
