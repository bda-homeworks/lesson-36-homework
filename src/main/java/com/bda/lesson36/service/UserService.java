package com.bda.lesson36.service;

import com.bda.lesson36.model.User;
import com.bda.lesson36.web.dto.UserRegistrationDTO;

public interface UserService {
    User save(UserRegistrationDTO userRegistrationDTO);
}
